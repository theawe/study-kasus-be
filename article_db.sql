/*
 Navicat Premium Data Transfer

 Source Server         : localhost_5432
 Source Server Type    : PostgreSQL
 Source Server Version : 130003 (130003)
 Source Host           : localhost:5432
 Source Catalog        : article
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 130003 (130003)
 File Encoding         : 65001

 Date: 31/03/2023 10:48:44
*/


-- ----------------------------
-- Sequence structure for posts_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "posts_id_seq";
CREATE SEQUENCE "posts_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS "posts";
CREATE TABLE "posts" (
  "id" int8 NOT NULL DEFAULT nextval('posts_id_seq'::regclass),
  "title" varchar(200) COLLATE "pg_catalog"."default",
  "content" text COLLATE "pg_catalog"."default",
  "category" varchar(100) COLLATE "pg_catalog"."default",
  "created_date" timestamp(6),
  "updated_date" timestamp(6),
  "status" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of posts
-- ----------------------------
BEGIN;
INSERT INTO "posts" ("id", "title", "content", "category", "created_date", "updated_date", "status") VALUES (2, 'Ayo kita belajar programming', 'Ini adalah konten belajar programming hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world ', 'Edukasi', NULL, NULL, 'status');
INSERT INTO "posts" ("id", "title", "content", "category", "created_date", "updated_date", "status") VALUES (4, 'Ayo kita belajar programming', 'Ini adalah konten belajar programming hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world ', 'Edukasi', '2023-03-31 09:17:14.224', '2023-03-31 09:17:14.224', 'draft');
INSERT INTO "posts" ("id", "title", "content", "category", "created_date", "updated_date", "status") VALUES (5, 'Ayo kita belajar programming', 'Ini adalah konten belajar programming hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world ', 'Edukasi', '2023-03-31 09:56:48.351', '2023-03-31 09:56:48.351', 'draft');
INSERT INTO "posts" ("id", "title", "content", "category", "created_date", "updated_date", "status") VALUES (0, 'Ayo kita belajar programming berasma', 'Ini adalah konten belajar programming hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world ', 'Edukasi', NULL, '2023-03-31 10:00:19.813', 'draft');
INSERT INTO "posts" ("id", "title", "content", "category", "created_date", "updated_date", "status") VALUES (3, 'Ayo kita belajar programming berasma', 'Ini adalah konten belajar programming hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world hello world ', 'Edukasi', '2023-03-31 09:06:34.616', '2023-03-31 10:03:22.393', 'draft');
INSERT INTO "posts" ("id", "title", "content", "category", "created_date", "updated_date", "status") VALUES (6, 'Dibidik Liverpool dan Bayern Munchen, Chelsea Segera Pasang Harga untuk Mason Mount', 'Rumor hengkangnya Mason Mount dari Chelsea di musim panas 2023 nanti terus berlanjut. The Blues kini telah menetapkan harga untuk sang pemain. Mount sampai sekarang masih belum menemui titik terang terkait kontrak barunya di Chelsea. Padahal, kontraknya akan habis pada 2024 mendatang. Situasi ini dimanfaatkan oleh Liverpool dan Bayern Munchen untuk mendekat. Kedua tim tertarik untuk mendatangkan pemain timnas Inggris tersebut.', 'Sport', '2023-03-31 10:21:31.685', '2023-03-31 10:21:31.685', 'draft');
INSERT INTO "posts" ("id", "title", "content", "category", "created_date", "updated_date", "status") VALUES (7, 'Dibidik Liverpool dan Bayern Munchen, Chelsea Segera Pasang Harga untuk Mason Mount', 'Rumor hengkangnya Mason Mount dari Chelsea di musim panas 2023 nanti terus berlanjut. The Blues kini telah menetapkan harga untuk sang pemain. Mount sampai sekarang masih belum menemui titik terang terkait kontrak barunya di Chelsea. Padahal, kontraknya akan habis pada 2024 mendatang. Situasi ini dimanfaatkan oleh Liverpool dan Bayern Munchen untuk mendekat. Kedua tim tertarik untuk mendatangkan pemain timnas Inggris tersebut.', 'Sport', '2023-03-31 10:31:08.368', '2023-03-31 10:31:08.368', 'publish');
COMMIT;

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "posts_id_seq"
OWNED BY "posts"."id";
SELECT setval('"posts_id_seq"', 7, true);

-- ----------------------------
-- Primary Key structure for table posts
-- ----------------------------
ALTER TABLE "posts" ADD CONSTRAINT "posts_pkey" PRIMARY KEY ("id");
